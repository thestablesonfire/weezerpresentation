import {Star} from './star';
import {Location} from './location';

export class Planet extends Star {
    public selected: boolean;
    public displayed: boolean;

    constructor(
        public location: Location,
        public name: string,
        public image: string,
        public bulletPoints: string[]
    ) {
        super(location);
        this.selected = false;
        this.displayed = true;
    }
}
