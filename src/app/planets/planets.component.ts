import { Component, OnInit } from '@angular/core';
import {Planet} from '../planet';
import {Location} from '../location';

@Component({
    selector: 'app-planets',
    templateUrl: './planets.component.html',
    styleUrls: ['./planets.component.scss']
})
export class PlanetsComponent implements OnInit {
    planets: Planet[];

    constructor() {}

    ngOnInit() {
        this.makePlanets();
    }

    makePlanets() {
        const vle = new Planet(
            new Location(0, 400),
            'VLE',
            'assets/vle.png',
            [
                'Fixed some totals calculations',
                'Cleaned up fees page',
                'Can now add and remove new fees',
                'Caused a bug where $0 fees weren\'t being sent',
                'Caused a bug where unset fees were being overwritten'
            ]
        );

        const lenderPrice = new Planet(
            new Location(500, 100),
            'Lender Price',
            'assets/lenderprice.png',
            [
                'Demoed to AEs -- they were really into it',
                'Now parsing credit info from 3.2 files'
            ]
        );

        const jsonSchema = new Planet(
            new Location(900, 300),
            'JSON Schema',
            'assets/jsonSchema.png',
            [
                'Lots of progress on JSON schema',
                'Address',
                'Asset',
                'Client',
                'Declarations',
                'Employment',
                'Income',
                'Liability',
                'Loan Details',
                'Loan Info',
                'Pricing Scenario',
                '1003 (FNMA)'
            ]
        );

        const einstein = new Planet(
            new Location(400, 500),
            'Einstein',
            'assets/einstein.png',
            [
                'Now in test!'
            ]
        );

        const sanl = new Planet(
            new Location(0, 100),
            'SaNL',
            'assets/ufo.png',
            [
                'Shout-outs to everyone',
                'Completed 12 stories in Weezer',
                '4 stories delayed',
                'Getting a new team member!',
                'Committed to 15 stories for Yes',
                'Prioritizing VLE/Fees and Tax Service'
            ]
        );

        this.planets = [sanl, vle, lenderPrice, jsonSchema, einstein];
    }

    selectPlanet(planet: Planet) {
        this.hidePlanets();
        this.deselectPlanets();
        planet.selected = true;
    }

    deselectPlanets() {
        for (const planet of this.planets) {
            planet.selected = false;
        }
    }

    hidePlanets() {
        for (const planet of this.planets) {
            planet.displayed = false;
        }
    }

    showAllPlanets() {
        for (const planet of this.planets) {
            planet.displayed = true;
            planet.selected = false;
        }
    }
}
