import {Location} from './location';

export class Star {
    public color: string;

    colors = [
        '#F00',
        '#0FF',
        '#FF0',
        '#FFF',
        '#FFF',
        '#FFF',
        '#FFF',
    ];

    constructor(public location: Location) {
        this.color = this.getRandomColor();
    }

    getRandomColor() {
        const len = this.colors.length - 1;

        return this.colors[Math.floor(Math.random() * len)];
    }
}
