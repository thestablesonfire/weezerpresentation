import { Component, OnInit } from '@angular/core';
import {Location} from '../location';
import {Star} from '../star';

@Component({
    selector: 'app-stars',
    templateUrl: './stars.component.html',
    styleUrls: ['./stars.component.scss']
})
export class StarsComponent implements OnInit {
    numStars = 200;
    screenWidth = 1900;
    screenHeight = 1500;

    stars: Star[];

    static getRandNumberBetween(min: number, max: number) {
        return Math.floor(Math.random() * max) + min;
    }

    constructor() {
        this.stars = [];
    }

    ngOnInit() {
        this.generateStars();
    }

    generateStars() {
        let newStar;

        for (let i = 0; i < this.numStars; i++) {
            newStar = new Star(new Location(
                StarsComponent.getRandNumberBetween(0, this.screenWidth),
                StarsComponent.getRandNumberBetween(0, this.screenHeight)
            ));

            this.stars.push(newStar);
        }
    }
}
